package com.example.penghitungzakat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.cv_zakat_fitrah)
    CardView cvZakatFitrah;
    @BindView(R.id.cv_zakat_mal)
    CardView cvZakatMal;
    @BindView(R.id.cv_zakat_hasil_pertanian)
    CardView cvZakatHasilPertanian;
    @BindView(R.id.cv_zakat_profesi)
    CardView cvZakatProfesi;
    @BindView(R.id.cv_zakat_Emas_Perak)
    CardView cvZakatEmasPerak;
    @BindView(R.id.cv_zakat_perdagangan)
    CardView cvZakatPerdagangan;
    @BindView(R.id.cv_bantuan)
    CardView cvBantuan;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        context = this;
    }

    @OnClick({R.id.cv_zakat_fitrah, R.id.cv_zakat_mal, R.id.cv_zakat_hasil_pertanian, R.id.cv_zakat_profesi, R.id.cv_zakat_Emas_Perak, R.id.cv_zakat_perdagangan, R.id.cv_bantuan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cv_zakat_fitrah:
                startActivity(new Intent(context, ZakatFitrahActivity.class));
                break;
            case R.id.cv_zakat_mal:
                startActivity(new Intent(context, ZakatMalActivity.class));
                break;
            case R.id.cv_zakat_hasil_pertanian:
                startActivity(new Intent(context, ZakatHasilPertanianActivity.class));
                break;
            case R.id.cv_zakat_profesi:
                startActivity(new Intent(context, ZakatProfesiActivity.class));
                break;
            case R.id.cv_zakat_Emas_Perak:
                startActivity(new Intent(context, ZakatEmasPerakActivity.class));
                break;
            case R.id.cv_zakat_perdagangan:
                startActivity(new Intent(context, ZakatPerdaganganActivity.class));
                break;
            case R.id.cv_bantuan:
                Toast.makeText(context, "Tahap pengembangan", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
