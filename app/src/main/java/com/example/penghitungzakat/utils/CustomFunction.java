package com.example.penghitungzakat.utils;

import android.widget.EditText;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class CustomFunction {

    public static int hitungZakatFitrah(double totalOrang) {
        //sumber : https://kumparan.com/berita-hari-ini/ini-ukuran-zakat-fitrah-menurut-mui-1tRcrYa0qPg/full
        double nominalZakat = 40600;
        return (int) Math.round(nominalZakat * totalOrang);
    }

    public static int hitungZakatMal(double uang, double saham, boolean includeZakat) {
        //rumus (uang tunai + saham) x 2,5%
        double zakat = 1;
        if (includeZakat){
            zakat = 0.025;
        }
        return (int) Math.round((uang + saham) * zakat);
    }

    public static int hitungZakatEmas(double beratEmas, double hargaEmas,boolean includeZakat) {
        double zakat = 1;
        if (includeZakat){
            zakat = 0.025;
        }
        return (int) Math.round(beratEmas * hargaEmas * zakat);
    }

    public static int hitungZakatPerak(double beratPerak, double hargaPerak,boolean includeZakat) {
        double zakat = 1;
        if (includeZakat){
            zakat = 0.025;
        }
        return (int) Math.round(beratPerak * hargaPerak * zakat);
    }

    public static int hitungZakatPertanian(double panen, double harga, boolean irigasiDisiram,boolean includeZakat) {
        double zakat = 1;
        if (irigasiDisiram) {
            if (includeZakat){
                zakat = 0.05;
            }
            return (int) Math.round(panen * harga * zakat);
        } else {

            if (includeZakat){
                zakat = 0.10;
            }
            return (int) Math.round(panen * harga * zakat);
        }
    }

    public static int hitungZakatProfesi(double pendapatan, double bonus, boolean includeZakat) {
        if (includeZakat){
            return (int) Math.round((pendapatan * 0.025) + (bonus * 0.10));
        }else{
            return (int) Math.round(pendapatan+bonus);
        }
    }

    public static  int hitungZakatPerdagangan(double modal, double keuntungan, double gajiPegawai,boolean includeZakat) {
        double zakat = 1;
        if (includeZakat){
            zakat = 0.025;
        }
        return (int) Math.round((modal + keuntungan - gajiPegawai) * zakat);
    }

    //fungsi convert ke rupiah
    public static String rupiahFormat(int value) {
        if (value<0){
            value = 0;
        }
        NumberFormat formatter = new DecimalFormat("#,###");
        return "Rp " + formatter.format(value);
    }

    public static double convetRpToDouble(EditText editText){
        String value = editText.getText().toString();
        value = value.replace("Rp ","");
        value = value.replace(".","");
        return Double.parseDouble(value);
    }

    public static double convetKgToDouble(EditText editText){
        String value = editText.getText().toString();
        value = value.replace(" Kg","");
        value = value.replace(".","");
        return Double.parseDouble(value);
    }

    public static double convetGrToDouble(EditText editText){
        String value = editText.getText().toString();
        value = value.replace(" gr","");
        value = value.replace(".","");
        return Double.parseDouble(value);
    }
}
