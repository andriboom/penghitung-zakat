package com.example.penghitungzakat;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.penghitungzakat.utils.CustomFunction;
import com.example.penghitungzakat.utils.GramTextWatcher;
import com.example.penghitungzakat.utils.RupiahTextWatcher;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ZakatEmasPerakActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edt_total_emas)
    EditText edtTotalEmas;
    @BindView(R.id.edt_harga_emas)
    EditText edtHargaEmas;
    @BindView(R.id.edt_total_perak)
    EditText edtTotalPerak;
    @BindView(R.id.edt_harga_perak)
    EditText edtHargaPerak;
    @BindView(R.id.tv_zakat_emas)
    TextView tvZakatEmas;
    @BindView(R.id.tv_zakat_perak)
    TextView tvZakatPerak;
    @BindView(R.id.tv_total_bayar)
    TextView tvTotalBayar;
    @BindView(R.id.tv_total_harga_emas)
    TextView tvTotalHargaEmas;
    @BindView(R.id.tv_total_harga_perak)
    TextView tvTotalHargaPerak;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zakat_emas_perak);
        ButterKnife.bind(this);
        //init toolbar
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Zakat Emas & Perak");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());
        //implement editext format text
        edtTotalEmas.addTextChangedListener(new GramTextWatcher(edtTotalEmas));
        edtTotalPerak.addTextChangedListener(new GramTextWatcher(edtTotalPerak));
        edtHargaPerak.addTextChangedListener(new RupiahTextWatcher(edtHargaPerak));
        edtHargaEmas.addTextChangedListener(new RupiahTextWatcher(edtHargaEmas));
    }

    @OnClick(R.id.btn_hitung)
    public void onViewClicked() {
        int hargaEmas = 0, hargaPerak = 0, zakatEmas = 0, zakatPerak = 0;
        if ((!edtHargaEmas.getText().toString().isEmpty()) || (!edtTotalEmas.getText().toString().isEmpty())) {
            hargaEmas = CustomFunction.hitungZakatEmas(CustomFunction.convetGrToDouble(edtTotalEmas), CustomFunction.convetRpToDouble(edtHargaEmas), false);
            zakatEmas = CustomFunction.hitungZakatEmas(CustomFunction.convetGrToDouble(edtTotalEmas), CustomFunction.convetRpToDouble(edtHargaEmas), true);
        }
        if ((!edtHargaPerak.getText().toString().isEmpty()) || (!edtTotalPerak.getText().toString().isEmpty())) {
            hargaPerak = CustomFunction.hitungZakatPerak(CustomFunction.convetGrToDouble(edtTotalPerak), CustomFunction.convetRpToDouble(edtHargaPerak), false);
            zakatPerak = CustomFunction.hitungZakatPerak(CustomFunction.convetGrToDouble(edtTotalPerak), CustomFunction.convetRpToDouble(edtHargaPerak), true);
        }
        tvTotalHargaEmas.setText(CustomFunction.rupiahFormat(hargaEmas));
        tvTotalHargaPerak.setText(CustomFunction.rupiahFormat(hargaPerak));
        tvZakatEmas.setText(CustomFunction.rupiahFormat(zakatEmas));
        tvZakatPerak.setText(CustomFunction.rupiahFormat(zakatEmas));
        tvTotalBayar.setText(CustomFunction.rupiahFormat((zakatEmas + zakatPerak)));
    }
}
