package com.example.penghitungzakat;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.penghitungzakat.utils.CustomFunction;
import com.example.penghitungzakat.utils.RupiahTextWatcher;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ZakatProfesiActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edt_gaji)
    EditText edtGaji;
    @BindView(R.id.edt_bonus)
    EditText edtBonus;
    @BindView(R.id.tv_total_bayar)
    TextView tvTotalBayar;
    @BindView(R.id.tv_harta_zakat)
    TextView tvHartaZakat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zakat_profesi);
        ButterKnife.bind(this);
        //init toolbar
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Zakat Profesi");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());
        //implement editext format text
        edtGaji.addTextChangedListener(new RupiahTextWatcher(edtGaji));
        edtBonus.addTextChangedListener(new RupiahTextWatcher(edtBonus));
    }

    @SuppressLint("SetTextI18n")
    @OnClick(R.id.btn_hitung)
    public void onViewClicked() {
        double pendapatan = 0, bonus = 0;
        int totalBayar = 0, totalPenghasilan = 0;
        if (!edtGaji.getText().toString().isEmpty()) {
            pendapatan = CustomFunction.convetRpToDouble(edtGaji);
        }
        if (!edtBonus.getText().toString().isEmpty()) {
            bonus = CustomFunction.convetRpToDouble(edtBonus);
        }

        if (pendapatan != 0 && bonus != 0) {
            totalBayar = CustomFunction.hitungZakatProfesi(pendapatan, bonus, true);
            totalPenghasilan = CustomFunction.hitungZakatProfesi(pendapatan, bonus, false);
        }
        tvTotalBayar.setText(CustomFunction.rupiahFormat(totalBayar));
        tvHartaZakat.setText(CustomFunction.rupiahFormat(totalPenghasilan));
    }
}
