package com.example.penghitungzakat;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.penghitungzakat.utils.CustomFunction;
import com.example.penghitungzakat.utils.RupiahTextWatcher;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ZakatPerdaganganActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edt_modal)
    EditText edtModal;
    @BindView(R.id.edt_keuntungan)
    EditText edtKeuntungan;
    @BindView(R.id.edt_total_pegawai)
    EditText edtTotalPegawai;
    @BindView(R.id.tv_total_bayar)
    TextView tvTotalBayar;
    @BindView(R.id.tv_harta_zakat)
    TextView tvHartaZakat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zakat_perdagangan);
        ButterKnife.bind(this);
        //init toolbar
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Zakat Perdagangan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());
        // ZakatPerdaganganActivity
        edtModal.addTextChangedListener(new RupiahTextWatcher(edtModal));
        edtTotalPegawai.addTextChangedListener(new RupiahTextWatcher(edtTotalPegawai));
        edtKeuntungan.addTextChangedListener(new RupiahTextWatcher(edtKeuntungan));
    }

    @OnClick(R.id.btn_hitung)
    public void onViewClicked() {
        double modal = 0, keuntungan = 0, totalPegawai = 0;
        int totalBayar = 0, totalZakat = 0;
        if (!edtModal.getText().toString().isEmpty()) {
            modal = CustomFunction.convetRpToDouble(edtModal);
        }
        if (!edtKeuntungan.getText().toString().isEmpty()) {
            keuntungan = CustomFunction.convetRpToDouble(edtKeuntungan);
        }
        if (!edtTotalPegawai.getText().toString().isEmpty()) {
            totalPegawai = CustomFunction.convetRpToDouble(edtTotalPegawai);
        }
        if (modal != 0 && keuntungan != 0 && totalPegawai != 0) {
            totalBayar = CustomFunction.hitungZakatPerdagangan(modal, keuntungan, totalPegawai, true);
            totalZakat = CustomFunction.hitungZakatPerdagangan(modal, keuntungan, totalPegawai, false);
        }
        tvTotalBayar.setText(CustomFunction.rupiahFormat(totalBayar));
        tvHartaZakat.setText(CustomFunction.rupiahFormat(totalZakat));
    }
}
