package com.example.penghitungzakat;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.penghitungzakat.utils.CustomFunction;
import com.example.penghitungzakat.utils.RupiahTextWatcher;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ZakatMalActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edt_uang)
    EditText edtUang;
    @BindView(R.id.edt_saham)
    EditText edtSaham;
    @BindView(R.id.btn_hitung)
    Button btnHitung;
    @BindView(R.id.tv_total_bayar)
    TextView tvTotalBayar;
    @BindView(R.id.tv_harta_zakat)
    TextView tvHartaZakat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zakat_mal);
        ButterKnife.bind(this);
        //init toolbar
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Zakat Mal");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());
        //implement editext format text
        edtUang.addTextChangedListener(new RupiahTextWatcher(edtUang));
        edtSaham.addTextChangedListener(new RupiahTextWatcher(edtSaham));
    }

    @OnClick(R.id.btn_hitung)
    public void onViewClicked() {
        int totalZakat = 0,totalBayar = 0;
        double uang = 0, saham = 0;
        if (!edtUang.getText().toString().isEmpty()) {
            uang = CustomFunction.convetRpToDouble(edtUang);
        }
        if (!edtSaham.getText().toString().isEmpty()) {
            saham = CustomFunction.convetRpToDouble(edtSaham);
        }
        if (uang != 0 && saham != 0) {
            totalZakat = CustomFunction.hitungZakatMal(uang, saham,false);
            totalBayar = CustomFunction.hitungZakatMal(uang, saham,true);
        }
        tvHartaZakat.setText(CustomFunction.rupiahFormat(totalZakat));
        tvTotalBayar.setText(CustomFunction.rupiahFormat(totalBayar));
    }
}
