package com.example.penghitungzakat;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.penghitungzakat.utils.CustomFunction;
import com.example.penghitungzakat.utils.KilogramTextWatcher;
import com.example.penghitungzakat.utils.RupiahTextWatcher;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ZakatHasilPertanianActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edt_harga_unit)
    EditText edtHargaUnit;
    @BindView(R.id.edt_hasil_panen)
    EditText edtHasilPanen;
    @BindView(R.id.sp_jenis_irigasi)
    Spinner spJenisIrigasi;
    @BindView(R.id.tv_total_bayar)
    TextView tvTotalBayar;
    @BindView(R.id.tv_harta_zakat)
    TextView tvHartaZakat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zakat_hasil_pertanian);
        ButterKnife.bind(this);
        //init toolbar
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Zakat Hasil Pertanian");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());
        //implement editext format text
        edtHasilPanen.addTextChangedListener(new KilogramTextWatcher(edtHasilPanen));
        edtHargaUnit.addTextChangedListener(new RupiahTextWatcher(edtHargaUnit));
    }

    @OnClick(R.id.btn_hitung)
    public void onViewClicked() {
        boolean irigasiDisiram = false;
        int totalBayar = 0, totalZakat = 0;
        double hasilPanen = 0, harga = 0;
        if (spJenisIrigasi.getSelectedItemPosition() == 1) {
            irigasiDisiram = true;
        }
        if (!edtHargaUnit.getText().toString().isEmpty()) {
            harga = CustomFunction.convetRpToDouble(edtHargaUnit);
        }
        if (!edtHasilPanen.getText().toString().isEmpty()) {
            hasilPanen = CustomFunction.convetKgToDouble(edtHasilPanen);
        }

        if (hasilPanen != 0 && harga != 0) {
            totalBayar = CustomFunction.hitungZakatPertanian(hasilPanen, harga, irigasiDisiram, true);
            totalZakat = CustomFunction.hitungZakatPertanian(hasilPanen, harga, irigasiDisiram, false);
        }
        tvTotalBayar.setText(CustomFunction.rupiahFormat(totalBayar));
        tvHartaZakat.setText(CustomFunction.rupiahFormat(totalZakat));
    }
}
