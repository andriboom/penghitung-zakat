package com.example.penghitungzakat;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.penghitungzakat.utils.CustomFunction;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ZakatFitrahActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edt_banyak_orang)
    EditText edtBanyakOrang;
    @BindView(R.id.tv_total_bayar)
    TextView tvTotalBayar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zakat_fitrah);
        ButterKnife.bind(this);
        //init toolbar
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Zakat Fitrah");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

    @OnClick(R.id.btn_hitung)
    public void onViewClicked() {
        int totalBayar = 0;
        if (!edtBanyakOrang.getText().toString().isEmpty()) {
            totalBayar = CustomFunction.hitungZakatFitrah(Double.parseDouble(edtBanyakOrang.getText().toString()));
        }
        tvTotalBayar.setText(CustomFunction.rupiahFormat(totalBayar));
    }
}
